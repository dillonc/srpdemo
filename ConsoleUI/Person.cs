namespace ConsoleUI;

public interface IPerson
{
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
}

public class Person : IPerson
{
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
}
